#ifndef __MESSAGES_H__
#define __MESSAGES_H__

#include "types.h"
#include <string>

struct cJSON;  

std::string jsonToString(cJSON* json);

// Create server commands
cJSON * createJOIN(const char * name, const char * key);
cJSON * createTHROTTLE(const float throttle);
cJSON * createSWITCHLANE(eLanes lane);

cJSON * createCREATERACE(const char * name, const char * key, const char * track, const char * password, int playerCount);

cJSON * createPING();

// Decode server responses



#endif