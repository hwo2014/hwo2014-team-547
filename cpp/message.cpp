#include "message.h"
#include "cJSON/cJSON.h"
#include <cstdlib>

std::string jsonToString(cJSON * json)
{
	const char * string = cJSON_PrintUnformatted(json);
	std::string messageString(string);

	free((void*)string);
	cJSON_Delete(json);

	messageString+='\n';
	return messageString;
}


cJSON * createJOIN(const char *name, const char *key)
{
	cJSON * message = cJSON_CreateObject();

	cJSON_AddStringToObject(message, "msgType", "join");

	cJSON * data = cJSON_CreateObject();
	cJSON_AddStringToObject(data, "name", name);
	cJSON_AddStringToObject(data, "key", key);

	cJSON_AddItemToObject(message, "data", data);

	return message;
}

cJSON * createTHROTTLE(const float throttle)
{
	cJSON * message = cJSON_CreateObject();

	cJSON_AddStringToObject(message, "msgType", "throttle");
	cJSON_AddNumberToObject(message, "data", throttle);

	return message;
}

const char * kDirection[2] =
{
	"Left",
	"Right"
};

cJSON * createSWITCHLANE(eLanes lane)
{
	cJSON * message = cJSON_CreateObject();

	cJSON_AddStringToObject(message, "msgType", "switchLane");
	cJSON_AddStringToObject(message, "data", kDirection[lane]);

	return message;
}	

cJSON * createPING()
{
	cJSON * message = cJSON_CreateObject();

	cJSON_AddStringToObject(message, "msgType", "ping");

	return message;
}