#include <cstdio>
#include <cstdlib>

#include "message.h"
#include "connection.h"

const char * kBotName = nullptr;
const char * kBotKey = nullptr;

int main(int argc, char * argv[])
{
    if(argc <3)
    {
        printf("Usage: ./run serverAddress port");
    }

    const char * serverAddress = argv[1];
    const int serverPort = atoi(argv[2]);

    if(argc >= 4)
        kBotName = argv[3];
    else
        kBotName = getenv("BOTNAME");

    if(argc >= 5)
        kBotKey = argv[4];
    else
        kBotKey =  getenv("BOTKEY");


    // printf("Server: %s:%d\n", serverAddress, serverPort);
    connection * server = setupConnection(serverAddress, serverPort);
    if(!server)
    {
        return -1;
    }

    cJSON * msg = createJOIN(kBotName, kBotKey);

    std::string joinString = jsonToString(msg);
    sendMessage(server, joinString.c_str());
    printf("Join: %s\n", joinString.c_str());

 
    std::string reply= recieveMessage(server);
    printf("First Reply: %s\n", reply.c_str());
    while(reply.find("gameTick") == reply.npos)
    {
        reply = recieveMessage(server);
        printf("Reply: %s\n", reply.c_str());
    }
    printf("Starting engines!\n");
    while(reply.find("tournamentEnd") == reply.npos)
    {
        if(reply.find("gameTick"))
        {
             sendMessage(server, jsonToString(createTHROTTLE(0.4)));
        }
        else 
        {
            printf("%s\n", reply.c_str());
        }
         reply = recieveMessage(server);
    }

    printf("%s\n", reply.c_str());
    reply = recieveMessage(server);
    printf("%s\n", reply.c_str());
    closeConnection(server);
    return 0;
}
