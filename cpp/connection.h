#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include <string>

struct connection;

connection * setupConnection(const char * address, int port);
void closeConnection(connection* server);

void sendMessage(connection* server, const std::string& message);
std::string recieveMessage(connection * server);

#endif