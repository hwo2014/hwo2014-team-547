#include "connection.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

struct connection
{
	int socket;
};


connection * setupConnection(const char *address, int port)
{

	connection * newConnection= (connection*) malloc(sizeof(connection));

	newConnection->socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

 	int flag = 1;
         int result = setsockopt(newConnection->socket,            /* socket affected */
                                 IPPROTO_TCP,     /* set option at TCP level */
                                 TCP_NODELAY,     // name of option 
                                 (char *) &flag,  /* the cast is historical
                                                         cruft */
                                 sizeof(int));    /* length of option value */

	sockaddr_in destAddr;
	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(port);

	hostent * host = gethostbyname(address);
    struct in_addr ** addr_list = (struct in_addr **)host->h_addr_list;
    destAddr.sin_addr = *addr_list[0];


	assert(connect(newConnection->socket, (sockaddr *)&destAddr, sizeof(destAddr)) != -1);


	return newConnection;
}

void closeConnection(connection * server)
{

	shutdown(server->socket, SHUT_RDWR);
	free(server);
}

void sendMessage(connection  * server, const std::string &  message)
{
	send(server->socket, message.c_str(), message.size(), 0);
}

std::string unprocessed;
char readBuff[512] = {0};
std::string recieveMessage(connection *server)
{	
	std::string ret;

	if(unprocessed.empty() == false)
	{
		int endLine = unprocessed.find('\n');
		if(endLine == unprocessed.npos)
		{
			ret = unprocessed;
			unprocessed.clear();
		}
		else
		{
			ret.insert(ret.end(), unprocessed.begin(), unprocessed.begin()+ endLine +1);
			unprocessed = std::string(unprocessed.begin()+endLine+1, unprocessed.end());
		}
	}
	

	while(ret.empty() || ret.rfind('\n') == ret.npos)
	{
		int recvAmount = recv(server->socket, readBuff, 512, 0);
		if(recvAmount == 0)
			continue;
		if(recvAmount == -1)
		{
		}

		char * endLine = std::find(readBuff, readBuff + recvAmount, '\n');
		if(endLine - readBuff < 512)
			endLine++;
		ret.insert(ret.end(), readBuff, endLine);
		if(endLine - readBuff < recvAmount)
		{
			unprocessed.insert(unprocessed.end(), endLine+1, readBuff+recvAmount);
		}
	}

	return ret;
}
